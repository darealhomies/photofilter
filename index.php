<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Photofilter</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <nav class="navbar navbar-extended-sm navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Photofilter</a>
        <ul class="navbar-nav float-right">
            <?php
                if (isset($_SESSION["username"])) {
                    echo '
                    <li class="nav-item">
                        <a href="#" class="nav-link">' . $_SESSION["username"] . '</a>
                    </li>
                    <li class="nav-item">
                        <a href="logout.php" class="nav-link">Logout</a>
                    </li>
                    ';
                } else {
                    echo '
                    <li class="nav-item">
                        <a href="Login.php" class="nav-link">Login</a>
                    </li>
                    <li class="nav-item">
                        <a href="create-account.php" class="nav-link">Create account</a>
                    </li>        
                    ';
                }
            ?>
        </ul>
    </nav>
    <div class="container-fluid">
        <hr/>
        <div class="row">
        	<div class="col-md-7">
                <canvas id="imageCanvas" data-caman-hidpi=""></canvas>
            </div>
            <div class="col-md-5">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a class="nav-link" data-toggle="tab" href="#transform-tab">Transform</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tone-tab">Tone</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#effects-tab">Effects</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active container" id="transform-tab">
                        <div class="btn btn-block btn-dark" id="rotate-right-btn">Rotate right</div>
                        <div class="btn btn-block btn-dark" id="rotate-left-btn">Rotate left</div>
                        <div class="btn btn-block btn-dark" id="flip-hori-btn">Flip horizontally</div>
                        <div class="btn btn-block btn-dark" id="flip-vert-btn">Flip vertically</div>
                    </div>
                    <div class="tab-pane fade" id="tone-tab">
                        <div class="paramContainer">
                            <span class="paramName">Brightness: </span>
                            <input id="brightness" type="range" min="-50" max="50" value="0" name="brightness">
                        </div>
                        <div class="paramContainer">
                            <span class="paramName">Contrast: </span>
                            <input id="contrast" type="range" min="-50" max="50" value="0" name="contrast">
                        </div>
                        <div class="paramContainer">
                            <span class="paramName">Saturation: </span>
                            <input "btn btn-light" id="saturation" type="range" min="-100" max="100" value="0" name="saturation">
                        </div>
                        <div class="paramContainer">
                            <span class="paramName">Sharpen: </span>
                            <input id="sharpen" type="range" min="0" max="100" value="0" name="sharpen">
                        </div>
                        <div class="paramContainer">
                            <span class="paramName">Blur: </span>
                            <input id="blur" type="range" min="0" max="10" value="0" name="blur">
                        </div>
                    </div>
                    <div class="tab-pane fade active" id="effects-tab">
                        <div class="btn btn-block btn-dark" id="none-btn">None</div>
                        <div class="btn btn-block btn-dark" id="vintage-btn">Vintage</div>
                        <div class="btn btn-block btn-dark" id="nostalgia-btn">Nostalgia</div>
                        <div class="btn btn-block btn-dark" id="orangePeel-btn">Orange Peel</div>
                        <div class="btn btn-block btn-dark" id="herMajesty-btn">Her Majesty</div>
                        <div class="btn btn-block btn-dark" id="concentrate-btn">Concentrate</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<div class="row">
        			<div id="downloadContainer">
	                    <button class="btn btn-default greenButton" id="buttonDownloadImage">Download image</button>
	                    <select name="downloadSize" id="downloadSize">
	                        <option value="720x480">720x480</option>
	                        <option value="800x600">800x600</option>
	                        <option value="1280x720">1280x720</option>
	                        <option value="1920x1080">1920x1080</option>
	                    </select>
                	</div>
        		</div>
        		<div class="row">
        			<button id="buttonSaveImage" class="btn btn-default greenButton">Save image</button>
        		</div>
        		<div class="row">
        			<input type="file" id="imageInput" class="inputfile" accept=".jpg, .jpeg, .png" />
               		<label for="imageInput"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose image...</span></label>
        		</div>
            </div>
        </div>
    </div>

    <script src="vendor/jquery-3.3.1.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/caman/caman.full.js"></script>
    <script src="vendor/download.js"></script>
    <script src="js/main.js"></script>
</body>

</html>