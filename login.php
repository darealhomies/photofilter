<?php
    session_start();

    if (isset($_SESSION["username"])) {
        header("Location: index.php");
    }

    if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }

    $DATABASE_NAME = "app.sqlite";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
            if (isset($_POST["username"]) && isset($_POST["password"])) {
                $db = new SQLite3("app.sqlite", SQLITE3_OPEN_READWRITE);
                if ($db->lastErrorCode() != 0) {
                    die($db->lastErrorMsg());
                }
                $username = $db->escapeString($_POST["username"]);
                $password = $db->escapeString($_POST["password"]);
                $stmt = $db->prepare("SELECT * FROM users WHERE name = :name");
                $stmt->bindParam(":name", $username);
                $result = $stmt->execute();
                if ($db->lastErrorCode() != 0) {
                    die($db->lastErrorMsg());
                }
                if ($result->numColumns() == 0) {
                    header("Location: login.php?error_credentials");
                }
                while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                    echo var_dump($row);
                    if (!isset($row["user_id"])) {
                        continue;
                    }
                    if (!password_verify($password, $row["password"])) {
                        header("Location: login.php?error_credentials");
                    } else {
                        $_SESSION["username"] = $username;
                        break;
                    }
                }
                if (isset($_SESSION["username"])) {
                    header("Location: index.php");
                } else {
                    header("Location: login.php?error_credentials");
                }
                $db->close();            
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container centered-div">
        <div class="row col-6">
            <form action="login.php" method="post">
                <div class="form-group">
                    <label for="input-name">Username</label>
                    <input type="text" class="form-control" id="input-name" name="username" placeholder="Enter username">
                    <?php
                        if (isset($_GET["error_credentials"])) {
                            echo "<span class='text-danger'>Wrong username or password</span>";
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label for="input-password">Password</label>
                    <input type="password" class="form-control" id="input-password" name="password" placeholder="Password">
                </div>
                <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
                <button type="submit" class="btn btn-primary">Login</button>        
            </form>
        </div>
    </div>
</body>
</html>