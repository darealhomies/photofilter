<?php
    session_start();
    include_once("config.php");

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION["username"])) {
        if (isset($_POST["imageData"])) {
            $data = $_POST["imageData"];
            // format of data is something like data:image/jpeg;base64,/9j/4AAQSkZJRgABA....
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);            
            $output_file = "img/upload/" . uniqid() . '.' . explode('/', $type)[1];
            file_put_contents($output_file, $data);

            $db = new SQLite3(DATABASE_NAME);
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            // Getting userid of logged in user
            $stmt = $db->prepare("SELECT * FROM users WHERE name = :name");
            $stmt->bindParam(":name", $_SESSION["username"]);
            $result = $stmt->execute();
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            $user_id = $result->fetchArray(SQLITE3_ASSOC)["user_id"];
            // Inserting image path into database
            $stmt = $db->prepare("INSERT INTO images (user_id, path) VALUES (:user_id, :path)");
            $stmt->bindParam(":user_id", $user_id);
            $stmt->bindParam(":path", $output_file);
            $stmt->execute();
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            $db->close();
        }
    }
?>