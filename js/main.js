var loadedImage = null;
var confirmLoadMsg = "Are you sure you want to load another image?\nAny unsaved progress will be lost.";

$(document).ready(function() {
    Caman("#imageCanvas", "img/drop_files_here.png", function() {
        this.render();
    });
    $("#downloadContainer").hide();
    $("#buttonSaveImage").hide();
    $("#imageInput").change(function(e) {
        $("#imageCanvas").removeAttr("data-caman-id");
        if(loadedImage) {
            if (confirm(confirmLoadMsg)) {
                loadImage(this.files);
            }
        } else loadImage(this.files);
    });

    $("#imageCanvas").on({
        'dragover dragenter': function(event) {
            event.preventDefault();
            event.stopPropagation();
        },
        'drop': function(event) {
            event.stopPropagation();
            event.preventDefault();

            var dt = event.originalEvent.dataTransfer;
            var files = dt.files;
            $("#imageCanvas").removeAttr("data-caman-id");
            if(loadedImage) {
                if (confirm(confirmLoadMsg)) {
                    loadImage(files);
                }
            } else loadImage(files);
        }
    });

    $('input[type=range]').on("change", function(){
        if (loadedImage !== null) {
            var brightness = parseInt($("#brightness").val());
            var contrast = parseInt($("#contrast").val());
            var saturation = parseInt($("#saturation").val());
            var sharpen = parseInt($("#sharpen").val());
            var blur = parseInt($("#blur").val());
            Caman("#imageCanvas", loadedImage.src, function() {
                this.revert(false);
                this.brightness(brightness);
                this.contrast(contrast);                
                this.saturation(saturation);
                this.sharpen(sharpen);
                this.stackBlur(blur);
                this.render();
            });    
        }
    });

    $("#rotate-right-btn").on("click", function(){
        Caman("#imageCanvas", loadedImage.src, function(){
            this.revert(false);
            this.rotate(90);
            this.render();
        });
    });
    $("#rotate-left-btn").on("click", function(){
        Caman("#imageCanvas", loadedImage.src, function(){
            this.revert(false);
            this.rotate(-90);
            this.render();
        });
    });
    $("#flip-hori-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            var vertical = false;
            this.mirror(vertical);
            this.render();
        });

    });
    $("#flip-vert-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            var vertical = true;
            this.mirror(vertical);
            this.render();
        });
    });

    $("#none-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert();
        });
    });
    $("#vintage-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            this.vintage().render();
        });
        $("#vintage-btn").html("Rendering...");
        Caman.Event.listen("renderFinished", function () {
            $("#vintage-btn").html("Vintage");
        });
    });
    $("#nostalgia-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            this.nostalgia().render();
        });
        $("#nostalgia-btn").html("Rendering...");
        Caman.Event.listen("renderFinished", function () {
            $("#nostalgia-btn").html("Nostalgia");
        });
    });
    $("#orangePeel-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            this.orangePeel().render();
        });
        $("#orangePeel-btn").html("Rendering...");
        Caman.Event.listen("renderFinished", function () {
            $("#orangePeel-btn").html("Orange Peel");
        });
    });
    $("#herMajesty-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            this.herMajesty().render();
        });
        $("#herMajesty-btn").html("Rendering...");
        Caman.Event.listen("renderFinished", function () {
            $("#herMajesty-btn").html("Her Majesty");
        });
    });
    $("#concentrate-btn").on("click", function() {
        Caman("#imageCanvas", loadedImage.src, function() {
            this.revert(false);
            this.concentrate().render();
        });
        $("#concentrate-btn").html("Rendering...");
        Caman.Event.listen("renderFinished", function () {
            $("#concentrate-btn").html("Concentrate");
        });
    });

    $("#buttonSaveImage").on("click", function() {
        if (loadedImage) {
            Caman("#imageCanvas", function(){
                var image = this.toBase64();
                $.post("save_image.php", {imageData: image});
            })
        }
    });
    $("#buttonDownloadImage").on("click", function() {
        var res = $("#downloadSize").find(":selected").text().split("x");
        var width = parseInt(res[0]);
        var height = parseInt(res[1]);
        var c = document.createElement("canvas");
        c.width = width;
        c.height = height;
        var ctx = c.getContext("2d");
        var srcCanvas = document.getElementById("imageCanvas");
        ctx.drawImage(srcCanvas, 0, 0, width, height);
        download(c.toDataURL(), "image.png", "image/png");        
    });
});

function loadImage(files) {
    if (files && files[0]) {
        $("#imageCanvas").css("border-style", "none");
        var reader = new FileReader();
        reader.onload = function(e) {
            loadedImage = document.createElement("img");
            loadedImage.src = e.target.result;
            loadedImage.id = "loadedImage";
            loadedImage.onload = function() {
                Caman("#imageCanvas", loadedImage.src, function() {
                    this.render();
                });
            }
        }
       reader.readAsDataURL(files[0]);
       $("#downloadContainer").show();
       $("#buttonSaveImage").show();
    }
}