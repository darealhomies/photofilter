<?php
    session_start();
    $DATABASE_NAME = "app.sqlite";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            if (!file_exists($DATABASE_NAME)) {
                $db = create_database($DATABASE_NAME);
            } else {
                $db = new SQLite3("app.sqlite", SQLITE3_OPEN_READWRITE);
            }
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            $username = $db->escapeString($_POST["username"]);
            if (check_user_already_taken($db, $username)) {
                header("Location: create-account.php?error_name_taken");
            }
            $password = $db->escapeString($_POST["password"]);
            $password = password_hash($password, PASSWORD_DEFAULT);
            $stmt = $db->prepare("INSERT INTO users (name, password) values (:name, :password)");
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            $stmt->bindParam(":name", $username);
            $stmt->bindParam(":password", $password);
            $stmt->execute();
            if ($db->lastErrorCode() != 0) {
                die($db->lastErrorMsg());
            }
            $db->close();
            header("Location: create-account.php?success");
        }
    }

    /* Function checks if username is already in the database
    db: database connection
    username: escaped string with username
    */
    function check_user_already_taken($db, $username) {
        $stmt = $db->prepare("SELECT name FROM users WHERE name = :name");
        $stmt->bindParam(":name", $username);
        $result = $stmt->execute();
        return $result->numColumns() > 0;
    }

    function create_database($name) {
        $db = new SQLite3($name);
        $db->exec("CREATE TABLE users (
            user_id  INTEGER      PRIMARY KEY AUTOINCREMENT
                                  NOT NULL,
            name     VARCHAR (50) UNIQUE
                                  NOT NULL,
            password VARCHAR (50) NOT NULL
        );
        ");
        if ($db->lastErrorCode() != 0) {
            die($db->lastErrorMsg());
        }
        $db->exec("CREATE TABLE images (
            image_id INTEGER PRIMARY KEY AUTOINCREMENT
                             NOT NULL,
            user_id  INTEGER REFERENCES users (user_id) ON DELETE RESTRICT
                                                        ON UPDATE RESTRICT,
            path     VARCHAR NOT NULL
        );
        ");
        if ($db->lastErrorCode() != 0) {
            die($db->lastErrorMsg());
        }
        return $db;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container centered-div">
        <div class="row col-6">
            <form action="create-account.php" method="post">
                <div class="form-group">
                    <label for="input-name">Username</label>
                    <input type="text" class="form-control" id="input-name" name="username" placeholder="Enter username">
                    <?php
                        if (isset($_GET["error_name_taken"])) {
                            echo "<span class='text-danger'>Username already taken</span>";
                        } else if (isset($_GET["success"])) {
                            echo "<span class='text-success'>Account created</span>";
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label for="input-password">Password</label>
                    <input type="password" class="form-control" id="input-password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Create account</button>        
            </form>
        </div>
    </div>
</body>
</html>