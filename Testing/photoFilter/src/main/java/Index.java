import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class Index {

    @FindBy(xpath = "/html/body/div/div[1]/div[1]/h1")
    private WebElement title;

    @FindBy(xpath = "/html/body/div/div[1]/div[2]/ul/li")
    private List <WebElement> authors;

    @FindBy(xpath = "/html/body/div/div[2]/div[1]/div")
    private List <WebElement> parameters;

    private WebDriver webDriver;

    public Index(WebDriver driver){
        this.webDriver=driver;
    }

    public void checkTitle() {
        Assert.assertTrue(title.isDisplayed());
    }

    public void checkAuthors() {
        Assert.assertEquals("Calin", authors.get(0).getText());
        Assert.assertEquals("Beatrice", authors.get(1).getText());
        Assert.assertEquals("Bogdan", authors.get(2).getText());
    }
    public void checkParameters() {
        Assert.assertTrue(parameters.get(0).isDisplayed());
        Assert.assertTrue(parameters.get(1).isDisplayed());
        Assert.assertTrue(parameters.get(2).isDisplayed());
    }
}
