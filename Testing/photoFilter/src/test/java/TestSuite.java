import org.junit.Test;

public class TestSuite extends TestBase{

    @Test
    public void checkelElements(){
        index.checkTitle();
        index.checkAuthors();
        index.checkParameters();
    }
}
