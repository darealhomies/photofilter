import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class TestBase {
    private  static WebDriver webDriver;
    protected static Index index;
    @BeforeClass
    public static void before(){

        System.setProperty("webdriver.chrome.driver","A:/Desktop/proiect mds/photofilter/Testing/chromedriver.exe");
        webDriver=new ChromeDriver();
        webDriver.get("file:///A:/Desktop/proiect%20mds/photofilter/index.html");
        index = PageFactory.initElements(webDriver, Index.class);
    }

    @AfterClass
    public static void after(){
        //webDriver.close();
    }
}
